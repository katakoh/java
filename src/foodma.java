import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Objects;

public class foodma {
    private JPanel root;
    private JLabel toplabel;
    private JButton tempuraButton;
    private JButton karaageButton;
    private JButton gyozaButton;
    private JButton udonButton;
    private JButton yakisobaButton;
    private JButton ramenButton;
    private JTextPane orderedItemsTextPane;
    private JButton checkOutButton;
    private JLabel aa;
    private JRadioButton takeout8TaxRadioButton;
    private JRadioButton eatin10TaxRadioButton;
    int sum = 0;

    void order(String FoodName, int price){
        int confirmation = JOptionPane.showConfirmDialog (null,
                "Would you like to order " +FoodName+","+price+"yen?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION
        );

        if(confirmation == 0) {
            int katagiri = JOptionPane.showConfirmDialog (null,
                    "Would you like a large portion(+100yen) ?",
                    "Order Confirmation",
                    JOptionPane.YES_NO_OPTION
            );
            if(katagiri==0){
                price+=100;
            }
            String currentText = orderedItemsTextPane.getText();
            orderedItemsTextPane.setText(currentText +FoodName+ " " +price+ "yen" + "\n");
            JOptionPane.showMessageDialog(null, "Thank you for Ordering " +FoodName+
                    "! It will be received as soon as possible.");
            sum+=price;
            if(takeout8TaxRadioButton.isSelected()){
                aa.setText((int)(sum*1.08)+ " yen");
            }else if(eatin10TaxRadioButton.isSelected()){
                aa.setText((int)(sum*1.10)+ " yen");
            }else{
                aa.setText("total" +sum+ " yen");
            }

        }
    }
    public foodma() {
        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("tempura", 150);
            }
        });
        tempuraButton.setIcon(new ImageIcon(
                this.getClass().getResource("tempura.png")
        ));

        karaageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("karaage", 200);
            }
        });
        karaageButton.setIcon(new ImageIcon(
                this.getClass().getResource("karaage.png")
        ));

        gyozaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("gyoza", 250);
            }
        });
        gyozaButton.setIcon(new ImageIcon(
                this.getClass().getResource("餃子.png")
        ));

        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("udon", 300);
            }
        });
        udonButton.setIcon(new ImageIcon(
                this.getClass().getResource("udon.png")
        ));

        yakisobaButton.addActionListener(new ActionListener() {
            @Override

      public void actionPerformed(ActionEvent e) {
                order("yakisoba", 350);
            }
        });
        yakisobaButton.setIcon(new ImageIcon(
                this.getClass().getResource("yakisoba.png")
        ));
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("ramen", 400);
            }
        });
        ramenButton.setIcon(new ImageIcon(
                this.getClass().getResource("ramen.png")
        ));

        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to checkout?",
                        "Order Confirmation",
                        JOptionPane.YES_NO_OPTION
                );
                if(confirmation == 0) {
                    if(takeout8TaxRadioButton.isSelected()){
                        sum = (int)(sum*1.08);
                        JOptionPane.showMessageDialog(null, "Thank you. The total price is " +sum+
                                " yen.");
                        sum = 0;
                        aa.setText(sum+ " yen");
                        orderedItemsTextPane.setText(null);
                    }else if(eatin10TaxRadioButton.isSelected()){
                        sum = (int)(sum*1.10);
                        JOptionPane.showMessageDialog(null, "Thank you. The total price is " +sum+
                                " yen.");
                        sum = 0;
                        aa.setText(sum+ " yen");
                        orderedItemsTextPane.setText(null);
                    }else{
                        JOptionPane.showMessageDialog(null, "You need to choose [take out] or [eat in].");
                    }
                }
            }
        });
        takeout8TaxRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                aa.setText((int)(sum*1.08)+ " yen");
            }
        });
        eatin10TaxRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                aa.setText((int)(sum*1.10)+ " yen");
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("foodma");
        frame.setContentPane(new foodma().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}

